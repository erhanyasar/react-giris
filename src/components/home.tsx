import { GlobalStateProvider } from "../store/globalState";
import ContextAPI from "./contextAPI";
import ToggleButton from "./toggleButton";
import PhoneBook from './phoneBook';

export default function Home () {
    return (
        <>
            <ToggleButton />
            <GlobalStateProvider>
                <ContextAPI />
            </GlobalStateProvider>
            <PhoneBook />
        </>
    )
}