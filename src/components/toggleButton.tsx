import { Button, Stack } from "@mui/material";
import { useState } from "react";

export default function ToggleButton () {
    const [button, setButton] = useState('ON');

    return (
        <Stack flexDirection="row" justifyContent="center">
            <Button onClick={() => setButton(button === 'ON' ? 'OFF' : 'ON')}>
                {button}
            </Button>
        </Stack>
    )
}