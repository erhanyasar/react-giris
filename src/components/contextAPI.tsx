import { Button, Typography } from "@mui/material";
import { useGlobalState } from "../store/globalState";

export default function ContextAPI () {
    const {state, dispatch} = useGlobalState();

    return (
        <>
            <Typography variant="h6" component="h2">
                {`Favourite programming language: ${state.selectedLanguage}`}
            </Typography>
            <Button onClick={() =>
                dispatch({type: "toggleLanguage"})

            }>
                Toggle Language
            </Button>
        </>
    )
}