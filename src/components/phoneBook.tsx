import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';

const style = {
  table: {
    borderCollapse: 'collapse'
  },
  tableCell: {
    border: '1px solid gray',
    margin: 0,
    padding: '5px 10px',
    width: 'max-content',
    minWidth: '150px'
  },
  form: {
    container: {
      padding: '20px',
      border: '1px solid #F0F8FF',
      borderRadius: '15px',
      width: 'max-content',
      marginBottom: '40px'
    },
    inputs: {
      marginBottom: '5px'
    },
    submitBtn: {
      marginTop: '10px',
      padding: '10px 15px',
      border:'none',
      backgroundColor: 'lightseagreen',
      fontSize: '14px',
      borderRadius: '5px'
    }
  }
}

function PhoneBookForm({ initialForm, setInitialForm, handleSubmitRecord }) {
  return (
    <form onSubmit={e => { e.preventDefault() }} style={style.form.container}>
      <label>First name:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userFirstname'
        name='userFirstname'
        value={initialForm.firstName}
        onChange={e => setInitialForm(prevState => {
          return {
            ...prevState,
            firstName: e.target.value
          }
        })}
        type='text'
      />
      <br/>
      <label>Last name:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userLastname'
        name='userLastname'
        value={initialForm.lastName}
        onChange={e => setInitialForm(prevState => {
          return {
            ...prevState,
            lastName: e.target.value
          }
        })}
        type='text' 
      />
      <br />
      <label>Phone:</label>
      <br />
      <input
        style={style.form.inputs}
        className='userPhone' 
        name='userPhone'
        value={initialForm.phone}
        onChange={e => setInitialForm(prevState => {
          return {
            ...prevState,
            phone: e.target.value
          }
        })}
        type='text'
      />
      <br/>
      <input 
        style={style.form.submitBtn} 
        className='submitButton'
        type='submit' 
        value='Add User'
        onClick={handleSubmitRecord}
      />
    </form>
  )
}

function InformationTable({records}) {
  return (
    <table style={style.table} className='informationTable'>
      <thead> 
        <tr>
          <th style={style.tableCell}>First name</th>
          <th style={style.tableCell}>Last name</th>
          <th style={style.tableCell}>Phone</th>
        </tr>
      </thead>
      <tbody>
        {records?.map((record, index) => {
          return (
            <tr key={index}>
              <td style={style.tableCell}>{record.firstName}</td>
              <td style={style.tableCell}>{record.lastName}</td>
              <td style={style.tableCell}>{record.phone}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  );
}

export default function PhoneBook(props) {
  const [initialForm, setInitialForm] = useState({
    firstName: 'Coder',
    lastName: 'Byte',
    phone: '8885559999',
  });
  const [records, setRecords] = useState([]);

  const handleSubmitRecord = () => {
    setRecords(prevState => {
      const unsortedRecords = [ ...prevState, initialForm ],
        sortedRecords = unsortedRecords.sort((a, b) => a.lastName.localeCompare(b.lastName));

      return sortedRecords;
    })
    setInitialForm({ firstName: '', lastName: '', phone: '' });
  };

  return (
    <section>
      <PhoneBookForm
        initialForm={initialForm}
        setInitialForm={setInitialForm}
        handleSubmitRecord={handleSubmitRecord}
      />
      <InformationTable records={records} />
    </section>
  );
}