import { Dispatch, SetStateAction } from "react";

export type Language = "Javascript" | "Python";

export type InitialState = {
    languages: string[],
    selectedLanguage: string,
    setSelectedLanguage: Dispatch<SetStateAction<Language>>;
}

export type GlobalStateContext = {
    state: InitialState,
    dispatch: Dispatch<Action>
};

export type Action = {type: 'reset'} | {type: 'toggleLanguage'}