import { InitialState } from "./types";

export const initialState: InitialState = {
    languages: ["Javascript", "Python"],
    selectedLanguage: 'Javascript',
    setSelectedLanguage: () => {}
}