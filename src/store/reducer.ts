import { initialState } from "../utils/constants";
import { Action } from "../utils/types";

export const RootReducer = (state = initialState, action: Action) => {
    const {type} = action;

    switch(type) {
        case "toggleLanguage":
            return {
                ...state,
                selectedLanguage:
                    state.selectedLanguage === 'Python' ? 'Javascript' : 'Python'
            }
        case "reset":
            return initialState;
        default:
            return state;
    }
}