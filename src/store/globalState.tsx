import { createContext, ReactElement, useContext, useReducer } from "react";
import { initialState } from "../utils/constants";
import { type GlobalStateContext } from "../utils/types";
import { RootReducer } from "./reducer";

const GlobalStateContext = createContext<GlobalStateContext | null>(null);

export const useGlobalState = () => {
    const context = useContext(GlobalStateContext);

    if (!context) throw new Error('Global State is not in scope');

    return context;
}

export const GlobalStateProvider = ({children}: {children: ReactElement}) => {
    const [state, dispatch] = useReducer(RootReducer, initialState)

    return (
        <GlobalStateContext.Provider value={{state, dispatch}}>
            {children}
        </GlobalStateContext.Provider>
    )
}