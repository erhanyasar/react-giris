import { Box, Container, Typography } from "@mui/material";
import "./App.css";
import Home from "./components/home";

function App() {
  return (
    <Container maxWidth="lg">
      <Box sx={{display: 'flex', flexDirection: 'column', flexGrow: 1, gap: 5}}>
        <Typography variant="h4" component="h2">
          React TypeScript Examples
        </Typography>
        <Home />
      </Box>
    </Container>
  );
}

export default App;
