import { CircularProgress, createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { StrictMode, Suspense } from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <StrictMode>
    <Suspense fallback={<CircularProgress />}>
      <ThemeProvider theme={createTheme()}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </Suspense>
  </StrictMode>
);
